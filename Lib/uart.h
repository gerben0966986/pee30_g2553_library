/*
 * uart.h
 *
 *  Created on: 4 mei 2017
 *      Author: VersD
 */

#ifndef LIB_UART_H_
#define LIB_UART_H_

/*
 * \defgroup UART
 * @brief De USART module gebruiken voor UART
 *
 * Naast de USCI module beschikt de microcontroller ook over een USART module.
 * Deze seriele module wodt gebruikt als UART of als SPI connectie. Deze functies
 * dienen om de module als UART module te gebruiken
 *
 * De volgende stappen moeten worden ondernomen om UART te kunnen gebruiken
 *
 * + Intialiseer de module met de juiste baudrate @see uartInit
 * + Stuur data op @see uartSend
 *
 * @{
 *
 */
#include "clocks.h"

/*!
 * @brief Initialiseer de USART module
 *
 * Hiermee wordt de module en pins ingesteld en aangezet. Na de aanroep van deze functie
 * kan data worden verstuurd en ontvangen op de ingestelde baudrate. De module zelf
 * communiceert over pinnen P1.1(rx) en P1.2(tx), deze kunnen dus niet langer voor IO gebruikt worden.
 *
 * \param clockSource Welke klok moet de USART module gebruiken?
 * \param baudrate Op welke baudrate moet de module communiceren? Standaard is 9600
  */
void uartInit(clockSources clockSource, uint16_t baudrate);

/*!
 * @brief Stel de communicatie baudrate in
 *
 * Op basis van de ingesteld klok en de gevraagde baudrate wordt de
 * interne baudrate generator ingesteld.
 *
 * \param clockSource Stel in welke klok-bron gebruikt moet worden voor module
 * \param baudrate Op welke baudrate moet de module communiceren? Standaard is 9600
 */
void uartSetBaudrate(clockSources clockSource, uint16_t baudrate);

/*!
 * @brief Stuur een byte aan data
 *
 * \param data De te versturen data. N.B. een computer terminal interpreteert alle data als karakters.
 */
void uartSend(uint8_t data);

/*
 * @}
 */
#endif /* LIB_UART_H_ */
